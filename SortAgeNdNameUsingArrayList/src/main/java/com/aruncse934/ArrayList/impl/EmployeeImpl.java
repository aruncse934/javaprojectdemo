package com.aruncse934.ArrayList.impl;

import com.aruncse934.ArrayList.Employee;

import java.util.ArrayList;
import java.util.Comparator;

public class EmployeeImpl {

    public static void main(String[] args) {

        ArrayList<Employee> employees = new ArrayList<Employee>();

        employees.add(new Employee(101,"Arun",3000,24,3));
        employees.add(new Employee(102,"Neha",2000,18,1));
        employees.add(new Employee(103,"Shruti",2000,27,2));
        employees.add(new Employee(104,"Vikash",4000,26,2));
        employees.add(new Employee(105,"Varun",5000,28,5));
        employees.add(new Employee(106,"Aakash",2000,20,1));

        System.out.println("Unsorted Employee Age and Name ::"+employees);

        System.out.println("===================================");
        System.out.println("Sorted Age");
        employees.sort(Comparator.comparingInt(Employee::getAge));
        for (Employee employee : employees){
            System.out.println("Employee Age::"+employee.getAge());
        }

        System.out.println("=============================");
        System.out.println("Sorted Name");
        employees.sort(Comparator.comparing(Employee::getName));
        for(Employee employee : employees){
            System.out.println("Employee Name::"+employee.getName());
        }

        System.out.println("==============================");
        System.out.println("Sorted Age And Name::");
        employees.sort(Comparator.comparingInt(Employee::getAge).thenComparing(Employee::getName));
        for (Employee employee:employees){
            System.out.println("Employee Age and Name::"+employee);
        }

        System.out.println("=======================================");
        System.out.println("Sorted Name and Age");
        employees.sort(Comparator.comparing(Employee::getName).thenComparingInt(Employee::getAge));
        for(Employee employee : employees){
            System.out.println("Employee Name and Age::"+employee);
        }

    }
}